# ContAgilWnd

PLUGIN AVALIATIVO
O PlugIn Avaliativo tem como objetivo selecionar candidatos para a equipe de desenvolvedores do projeto ContAgil.
A tarefa consiste em importar um arquivo SPED EFD, manipular, disponibilizar e exibir seus dados para o usuário.
Observações referentes ao desenvolvimento desse plugin avaliativo:
1- Foram importadas apenas as entidades EFD (cod "0000"), Participantes (cod "0150"), Nota Fiscal ISS (cod "A100"),
Item Nota Fiscal ISS (cod "A170"), Nota Fiscal ICMS (cod "C100"), Item Nota Fiscal ICMS (cod "C170").
2- Para cada entidade foram importados seus 6 campos considerados principais.
3- Tendo em vista que apenas uma EFD foi importada, decidiu-se pela apresentação dos dados dos participantes no
formulário paginado. Os dados foram ordenados pela UF e em seguida pelo nome do participante.
4- Por uma questão estética, foram retirados da apresentação gráfica e das tabelas os participantes cujos códigos 
do município vieram sem preenchimento. No MAD eles são apresentados.
5- A implementação dos métodos "hashCode()", "equals()" e "compareTo()", foram implementados de forma simplificada
para esse exercício.
