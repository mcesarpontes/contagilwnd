package gov.rfb.contagil.sped.efd.app;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.guga.contagil.acoes.Acao;
import org.guga.contagil.acoes.Menu;
import org.guga.contagil.acoes.OpenDocumentsManager;
import org.guga.contagil.acoes.mads.RefModeloAnaliticoDinamicoStrategy;
import org.guga.contagil.analitico.AnLeiaute;
import org.guga.contagil.plugins.ContAgilPlugInAbstract;
import org.guga.contagil.plugins.SpecEntidade;
import org.guga.contagil.projeto.TipoProjeto;

import gov.rfb.contagil.commons.tipos.ScriptingObject;
import gov.rfb.contagil.sped.efd.EFD;
import gov.rfb.contagil.sped.efd.acoes.AcaoExibirCRUD;
import gov.rfb.contagil.sped.efd.acoes.AcaoExibirResumo;
import gov.rfb.contagil.sped.efd.acoes.AcaoExibirSEARCH;
import gov.rfb.contagil.sped.efd.acoes.AcaoImportarEFD;
import gov.rfb.contagil.sped.efd.dao.EFDList;
import gov.rfb.contagil.sped.efd.scripting.EFDScript;

public class PlugIn extends ContAgilPlugInAbstract {

	/**
	 * Retorna o nome deste plugin.
	 */
	public String getNome() {

		return "Plugin EFD";
	}
  
	/**
	 * Retorna rela��o das entidades que podem ser acessadas via MAD
	 */
	@Override
	public SpecEntidade[] getSpecEntidades() {

		SpecEntidade[] specs = new SpecEntidade[1];

		specs[0] = new SpecEntidade();
		specs[0].setTipoEntidade(EFD.class);
		specs[0].setDirReference(EFDList.DIR_REF);
		specs[0].setContextoMADAutomatico("EFD", /* inclui_entidades_filhas */true, EFDList.class);

		return specs;
	}

	/**
	 * Retorna rela��o dos nomes das vari�veis que devem ser criadas em scripts
	 */
	@Override
	public Collection<String> getNomesVariaveisScript() {
System.out.println("algu�m chamou getNomesVari�veisSecript -------------------------");
		return Arrays.asList(new String[] { "EFD" });
	}

	/**
	 * Retorna implementa��es de vari�veis para scripts.
	 */
	@Override
	public ScriptingObject getInstanciaScript(String nome) {

		if ("EFD".equalsIgnoreCase(nome)) {
System.out.println("algu�m chamou getInstanciaScript -------------------------");
			return new EFDScript();
		} else {
			return null;
		}
	}

	/**
	 * Retorna o nome do arquivo serializado que cont�m a documenta��o da API dos
	 * objetos de script.
	 */
	@Override
	public String getScriptingAPIDocumentation() {

		return "plugin_efd.API";
	}
	
	@Override
	public String getScriptingAPIJavadocs() {
		return "pligin_efd.JAVADOCS";
	}

	/**
	 * Caso este plugin defina novos 'tipos de projetos', retornar aqui a rela��o
	 * dos tipos de projetos definidos pelo plugin. Caso contr�rio, retornar NULL ou
	 * uma lista vazia.<BR>
	 * OBS: caso algum 'tipo de projeto' definido pelo plugin seja espec�fico para
	 * determinado 'perfil' de usu�rio, o pr�prio plugin deve fazer esta verifica��o
	 * durante execu��o desta fun��o. No momento da execu��o desta fun��o espera-se
	 * que os 'perfis' do usu�rio j� tenham sido definidos pela aplica��o.<BR>
	 */
	@Override
	public List<TipoProjeto> getTiposProjetos() {
		return null;

	}

	/**
	 * Caso este plugin defina novos 'menus', retornar aqui a rela��o desses novos
	 * menus. Caso algum desses menus tenha o mesmo t�tulo que outro menu existente
	 * na aplica��o, junta os itens de menu definidos por este plugin aos demais
	 * itens j� definidos em outra parte da aplica��o.<BR>
	 * 
	 * @param man         Gerenciador que � utilizado para informar sobre a
	 *                    exist�ncia de dados dispon�veis na janela principal da
	 *                    aplica��o (isso � importante para os menus do MAD).
	 * @param acoes       Mapa que recebe a rela��o de a��es criadas por esta
	 *                    fun��o. A chave do mapa � um objeto do tipo Acao, o valor
	 *                    � o correspondente JMenuItem da a��o.
	 * @param mads        Lista que recebe as configura��es de "Modelo Anal�tico
	 *                    Din�mico" que foram definidas no contexto deste menu. Cada
	 *                    elemento da lista � um objeto do tipo
	 *                    RefModeloAnaliticoDinamicoStrategy.
	 * @param mapLeiautes Mapa que recebe a rela��o de leiautes carregados para este
	 *                    MAD (leiautes padronizados do Cont�gil e tamb�m leiautes
	 *                    customizados criados pelo usu�rio).
	 * @return Retorna a rela��o de novos menus, ou retornar NULL caso n�o tenha
	 *         novos menus a apresentar.
	 */
	@Override
	public List<JMenu> getMenus(OpenDocumentsManager man, Map<Acao, JMenuItem> acoes,
			List<RefModeloAnaliticoDinamicoStrategy> mads, Map<String, AnLeiaute> mapLeiautes) {

		List<JMenu> menus = new LinkedList<>();

		/* Define menu principal "SPED EFD" */
		JMenu menuSPED_EFD = new JMenu("SPED EFD");
		menus.add(menuSPED_EFD);
		Menu.addAcao(menuSPED_EFD, new AcaoImportarEFD(), man, acoes);

		// Define sub menu "CRUD"
		JMenu subMenuCRUD = new JMenu("CRUD");
		menuSPED_EFD.add(subMenuCRUD);
		Menu.addAcao(subMenuCRUD, new AcaoExibirCRUD(), man, acoes);
		
		// Define sub menu "SEARCH"
		JMenu subMenuSEARCH = new JMenu("SEARCH");
		menuSPED_EFD.add(subMenuSEARCH);
		Menu.addAcao(subMenuSEARCH, new AcaoExibirSEARCH(), man, acoes);
		
		// Define sub menu "RESUMO"
		JMenu subMenuResumo = new JMenu("RESUMO");
		menuSPED_EFD.add(subMenuResumo);
		Menu.addAcao(subMenuResumo, new AcaoExibirResumo(), man, acoes);

		return menus;
	}
}
