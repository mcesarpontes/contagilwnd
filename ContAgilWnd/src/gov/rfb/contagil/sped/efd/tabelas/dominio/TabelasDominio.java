package gov.rfb.contagil.sped.efd.tabelas.dominio;

import org.guga.contagil.tabelas.dominio.TabelaDominioComum;

import gov.rfb.contagil.commons.utils.Persistencia;
/**
 * Define as Tabelas Dom�nio usadas neste EFD.
 * 
 * 
 * @author Marcus Cesar Pontes
 *
 */
public class TabelasDominio {

	@Persistencia.TabelaDominio("ATIVIDADE")
	public static class ATIVIDADE extends TabelaDominioComum {

		private static TabelaDominioComum instancia = null;

		@Persistencia.Coluna
		@Persistencia.Chave
		private int codigoAtividade;

		@Persistencia.Coluna
		private String descricaoAtividade;

		public ATIVIDADE() {
			
			super("gov.rfb.contagil.sped.efd.res.ATIVIDADE.txt", 0, 0, 2);
		}

		public static TabelaDominioComum getDefault() {

			if (instancia == null)
				instancia = new ATIVIDADE();
			return instancia;
		}

	}
	
	
	@Persistencia.TabelaDominio("MUNICIPIO")
	public static class MUNICIPIO extends TabelaDominioComum {

		private static TabelaDominioComum instancia = null;

		@Persistencia.Coluna
		@Persistencia.Chave
		private int codigoMunicipio;

		@Persistencia.Coluna
		private String nomeMunicipio;

		public MUNICIPIO() {
			
			super("gov.rfb.contagil.sped.efd.res.MUNICIPIO.xls", 0, 0, 2);
		}

		public static TabelaDominioComum getDefault() {

			if (instancia == null)
				instancia = new MUNICIPIO();
			return instancia;
		}

	}

	
	@Persistencia.TabelaDominio("CODIGOSUF")
	public static class CODIGOSUF extends TabelaDominioComum {

		private static TabelaDominioComum instancia = null;

		@Persistencia.Coluna
		@Persistencia.Chave
		private int codigoUF;

		@Persistencia.Coluna
		private String nomeUF;

		public CODIGOSUF() {
			
			super("gov.rfb.contagil.sped.efd.res.CODIGOSUF.xls", 0, 0, 2);
		}

		public static TabelaDominioComum getDefault() {

			if (instancia == null)
				instancia = new CODIGOSUF();
			return instancia;
		}

	}
	
	
	
	
	
	
	
	
	
	
	
}
