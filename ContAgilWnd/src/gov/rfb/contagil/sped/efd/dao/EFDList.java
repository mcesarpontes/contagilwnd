package gov.rfb.contagil.sped.efd.dao;

import org.guga.contagil.importa.DadosSistemasPersistenciaComum;
import org.guga.contagil.projeto.workdir.DadosSistemasDirReference;

import gov.rfb.contagil.sped.efd.EFD;
/**
 * Classe que cria e permite o acesso ao arquivo no qual os dados <br>
 * das entidades consideradas neste EFD ser�o persistidos.
 *
 * @author Marcus Cesar Pontes
 *
 */
public class EFDList extends DadosSistemasPersistenciaComum{

	private static final String EXTENSAO = "SPED_EFD";
	
	protected Class<?> getTipoDadoPersistente() {
		
		return EFD.class;
	}
		
	public static DadosSistemasDirReference DIR_REF =
			DadosSistemasDirReference.getDirReferenceParaEntidade("EFD",EFD.class,EXTENSAO);
	
	/* ????? DIR_REF � static*/
	public String getPath() {
		return DIR_REF.toString();
	}
		
	public String getExtensao() {
		return EXTENSAO;
	}
	
	public String getField() {
		return "ARQUIVO SPED EFD";
	}
	
	public EFDList() {
		super(DIR_REF);
	}
	
}
