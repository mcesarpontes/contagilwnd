package gov.rfb.contagil.sped.efd;

import org.guga.contagil.model.ComparadorGenerico;

import gov.rfb.contagil.commons.tipos.AnGrao;
import gov.rfb.contagil.commons.tipos.ReflexiveType;

/**
 * 
 * Informa��es obre todos os itens de uma EFD
 * 
 * Esta entidade n�o foi implementada neste exerc�cio
 * 
 * Bloco 0 - Registro 0200
 *  
 * @author Marcus Cesar
 *
 */
public class Item implements java.io.Serializable, AnGrao,ReflexiveType, Comparable<Item>{
	
	private static final long serialVersionUID = 1L; 
	
	String registroItem; // 4
	String codigoItem; // 60
	String descricaoItem; // 60
	String codigoBarraItem; // 60
	String codigoAnteriorItem;// 60
	String unidadeInventarioItem;// 6
	String tipoItem;// 2 tabelaDominio Tipo_Item
	String ncmItem; // 8
	String exIPIItem; // 3
	String codigoGeneroItem; // 2
	String codigoServicoItem;// 5
	int aliquotaICMSItem;// 2

	public String getRegistroItem() {
		return registroItem;
	}

	public void setRegistroItem(String registroItem) {
		this.registroItem = registroItem;
	}

	public String getCodigoItem() {
		return codigoItem;
	}

	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}

	public String getDescricaoItem() {
		return descricaoItem;
	}

	public void setDescricaoItem(String descricaoItem) {
		this.descricaoItem = descricaoItem;
	}

	public String getCodigoBarraItem() {
		return codigoBarraItem;
	}

	public void setCodigoBarraItem(String codigoBarraItem) {
		this.codigoBarraItem = codigoBarraItem;
	}

	public String getCodigoAnteriorItem() {
		return codigoAnteriorItem;
	}

	public void setCodigoAnteriorItem(String codigoAnteriorItem) {
		this.codigoAnteriorItem = codigoAnteriorItem;
	}

	public String getUnidadeInventarioItem() {
		return unidadeInventarioItem;
	}

	public void setUnidadeInventarioItem(String unidadeInventarioItem) {
		this.unidadeInventarioItem = unidadeInventarioItem;
	}

	public String getTipoItem() {
		return tipoItem;
	}

	public void setTipoItem(String tipoItem) {
		this.tipoItem = tipoItem;
	}

	public String getNcmItem() {
		return ncmItem;
	}

	public void setNcmItem(String ncmItem) {
		this.ncmItem = ncmItem;
	}

	public String getExIPIItem() {
		return exIPIItem;
	}

	public void setExIPIItem(String exIPIItem) {
		this.exIPIItem = exIPIItem;
	}

	public String getCodigoGeneroItem() {
		return codigoGeneroItem;
	}

	public void setCodigoGeneroItem(String codigoGeneroItem) {
		this.codigoGeneroItem = codigoGeneroItem;
	}

	public String getCodigoServicoItem() {
		return codigoServicoItem;
	}

	public void setCodigoServicoItem(String codigoServicoItem) {
		this.codigoServicoItem = codigoServicoItem;
	}

	public int getAliquotaICMSItem() {
		return aliquotaICMSItem;
	}

	public void setAliquotaICMSItem(int aliquotaICMSItem) {
		this.aliquotaICMSItem = aliquotaICMSItem;
	}
	
	
	public int compareTo(Item ref) {
		int comp = 0;
		if (this==ref) {
			return comp;
		}

		if (ref == null) {
			return +1;
		}

		comp = ComparadorGenerico.COMPARE(descricaoItem, ref.descricaoItem);
		if (comp != 0)
			return comp;

		return 0;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash = 37 * (hash + ComparadorGenerico.HASHCODE(descricaoItem));

		return 17 + hash;
	}

	@Override
	public boolean equals(Object o) {
		boolean equal = true;
		if (this == o) {
			return equal;
		}

		if (o == null) {
			return !equal;
		}

		if (!(o instanceof Item)) {
			return !equal;
		}

		Item ref  = (Item) o;
		equal = ComparadorGenerico.EQUALS(descricaoItem, ref.descricaoItem);
		if (!equal)
			return false;

		return equal;
	}

}
