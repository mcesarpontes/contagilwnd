package gov.rfb.contagil.sped.efd.parse;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import gov.rfb.contagil.sped.efd.EFD;
import gov.rfb.contagil.sped.efd.Item;
import gov.rfb.contagil.sped.efd.ItemICMS;
import gov.rfb.contagil.sped.efd.ItemISS;
import gov.rfb.contagil.sped.efd.NotaFiscalICMS;
import gov.rfb.contagil.sped.efd.NotaFiscalISS;
import gov.rfb.contagil.sped.efd.Participante;
import gov.rfb.contagil.sped.efd.dao.EFDList;

/**
 * Classe que cont�m os m�todos de parsing correspondentes a<br>
 * cada entidade considerada neste exerc�cio
 * 
 * @author Marcus Cesar Pontes
 *
 */

public class Parse {

	private static Parse instancia;

	/* Todas as entidades consideradas neste exerc�cio */
	private EFD efd;

	private Participante participante;

	private NotaFiscalICMS notaFiscalICMS;

	private ItemICMS itemNotaFiscalICMS;

	private NotaFiscalISS notaFiscalISS;

	private ItemISS itemNotaFiscalISS;

	private Item item;

	private Parse() {
	}

	public static Parse getInstanciaParse() {
		if (instancia==null)
			instancia = new Parse();
		return instancia;
	}

	/**
	 * M�todo que encerra o procedimento de Parsing, c�digo "9999"
	 * persistindo a entidade principal: EFD 
	 * 
	 * @param msg - para ficar de acordo com os demais m�tos de parsing
	 * @throws IOException - Erro I/O
	 */
	public void EncerraParse(String msg) throws IOException {

		/* Persist�ncia da Entidade principal */

		new EFDList().writeFile(efd);

		/* Simula Persist�ncia do ContAgil para testes no Eclipse */
		//new DAO().writeFile(efd);

		// System.out.println(efd);
	}

	/**
	 * M�todo que executa o parsing da entidade EFD
	 * 
	 * Bloco 0 - Registro 0000
	 * 
	 * Para este exerc�cio foram considerados apenas 7 campos:
	 * 
	 * @param linhaCamposEntidade - cont�m as informa��es sobre esta entidade
	 */
	public void ParseEFD(String linhaCamposEntidade) {

		/* Instancia uma entidade Estabelecimento */
		efd = new EFD();

		/* Separa e ajusta as informa��es constantes em linhaCamposEntidade */
		String campos[] = getCamposLinhaEntidade(linhaCamposEntidade);

		/* Parse data */
		SimpleDateFormat formatador = new SimpleDateFormat("ddMMyyyy");

		Date data = new Date();
		try {
			data = formatador.parse(campos[6]);
			efd.setDataInicio(data);
			data = formatador.parse(campos[7]);
			efd.setDataFinal(data);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		efd.setNome(campos[8]);
		efd.setCnpj(Long.parseLong(campos[9]));
		efd.setUnidadeFederacao(campos[10]);
		efd.setCodigoMunicipio(Integer.parseInt(campos[11]));
		efd.setCodigoAtividade(Integer.parseInt(campos[14]));
	}

	/**
	 * M�todo que executa o parsing da entidade NotaFiscalICMS
	 * 
	 * Bloco C - Registro C100
	 * 
	 * Para este exerc�cio foram considerados apenas 6 campos
	 * 
	 * @param linhaCamposEntidade - cont�m as informa��es sobre esta entidade
	 */
	public void ParseNotaFiscalICMS(String linhaCamposEntidade) {

		/* Instancia nova NotaFiscalICMS */
		notaFiscalICMS = new NotaFiscalICMS();

		/* Separa e ajusta as informa��es constantes em linhaCamposEntidade */
		String campos[] = getCamposLinhaEntidade(linhaCamposEntidade);

		/* Inicializa atributos */
		notaFiscalICMS.setCodigoParticipante(campos[4]);
		notaFiscalICMS.setNumeroNota(campos[8]);
		notaFiscalICMS.setValorTotalNota(Double.valueOf(campos[12]));

		/* Parse data */
		SimpleDateFormat formatador = new SimpleDateFormat("ddMMyyyy");

		Date data = new Date();
		try {
			data = formatador.parse(campos[10]);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		notaFiscalICMS.setDataEmissao(data);
		notaFiscalICMS.setValorPIS(Double.valueOf(campos[26]));
		notaFiscalICMS.setValorCOFINS(Double.valueOf(campos[27]));

		/* Estabelece as refer�ncias Pai/Filho */
		notaFiscalICMS.setEFD(efd);
		efd.setNotaFiscalICMS(notaFiscalICMS);
	}

	/**
	 * M�todo que executa o parsing da entidade ItemNotaFiscalICMS.
	 * 
	 * Bloco C - Registro C170
	 * 
	 * Para este exerc�cio foram considerados apenas 6 campos.
	 * 
	 * Como o arquivo EFD � sequencial, este item corresponde � �ltima
	 * NotaFiscalICMS instanciada.
	 * 
	 * @param linhaCamposEntidade - cont�m as informa��es sobre esta entidade.
	 */
	public void ParseItemNotaFiscalICMS(String linhaCamposEntidade) {

		/* Instancia novo ItemNotaFiscalICMS */
		itemNotaFiscalICMS = new ItemICMS();

		/* Separa e ajusta as informa��es constantes em linhaCamposEntidade */
		String campos[] = getCamposLinhaEntidade(linhaCamposEntidade);

		/* Inicializa atributos */
		itemNotaFiscalICMS.setNumeroItem(campos[2]);
		itemNotaFiscalICMS.setCodigoItem(campos[3]);
		itemNotaFiscalICMS.setDescricaoItem(campos[4]);
		itemNotaFiscalICMS.setValorTotalItem(Double.valueOf(campos[7]));
		itemNotaFiscalICMS.setValorPIS(Double.valueOf(campos[30]));
		itemNotaFiscalICMS.setValorCOFINS(Double.valueOf(campos[36]));

		/* Estabelece as refer�ncias Pai/Filho */
		itemNotaFiscalICMS.setNotaICMS(notaFiscalICMS);
		notaFiscalICMS.addItemICMS(itemNotaFiscalICMS);
	}

	/**
	 * M�todo que executa o parsing da entidade NotaFiscalISS
	 * 
	 * Bloco A - Registro A100
	 * 
	 * Para este exerc�cio foram considerados apenas 6 campos
	 * 
	 * @param linhaCamposEntidade - cont�m as informa��es sobre esta entidade
	 */
	public void ParseNotaFiscalISS(String linhaCamposEntidade) {

		notaFiscalISS = new NotaFiscalISS();

		/* Separa e ajusta as informa��es constantes em linhaCamposEntidade */
		String campos[] = getCamposLinhaEntidade(linhaCamposEntidade);

		notaFiscalISS.setCodigoParticipante(campos[4]);
		notaFiscalISS.setNumeroNota(campos[8]);

		/* Parse data */
		SimpleDateFormat formatador = new SimpleDateFormat("ddMMyyyy");

		Date data = new Date();
		try {
			data = formatador.parse(campos[10]);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		notaFiscalISS.setDataEmissao(data);
		notaFiscalISS.setValorTotalNota(Double.valueOf(campos[12]));
		notaFiscalISS.setValorPIS(Double.valueOf(campos[16]));
		notaFiscalISS.setValorCOFINS(Double.valueOf(campos[18]));

		/* Estabelece as refer�ncias Pai/Filho */
		notaFiscalISS.setEFD(efd);
		efd.setNotaFiscalISS(notaFiscalISS);
	}

	/**
	 * M�todo que executa o parsing da entidade ItemNotaFiscalISS.
	 * 
	 * Bloco A - Registro A170
	 * 
	 * Para este exerc�cio foram considerados apenas 6 campos.
	 * 
	 * Como o arquivo EFD � sequencial, este item corresponde � �ltima NotaFiscalISS
	 * instanciada.
	 * 
	 * @param linhaCamposEntidade - cont�m as informa��es sobre esta entidade.
	 */
	public void ParseItemNotaFiscalISS(String linhaCamposEntidade) {

		itemNotaFiscalISS = new ItemISS();

		/* Separa e ajusta as informa��es constantes em linhaCamposEntidade */
		String campos[] = getCamposLinhaEntidade(linhaCamposEntidade);

		itemNotaFiscalISS.setNumeroItem(campos[2]);
		itemNotaFiscalISS.setCodigoItem(campos[3]);
		itemNotaFiscalISS.setDescricaoItem(campos[4]);
		itemNotaFiscalISS.setValorTotalItem(Double.valueOf(campos[5]));
		itemNotaFiscalISS.setValorPIS(Double.valueOf(campos[12]));
		itemNotaFiscalISS.setValorCOFINS(Double.valueOf(campos[16]));

		/* Estabelece as refer�ncias Pai/Filho */
		itemNotaFiscalISS.setNotaFiscalISS(notaFiscalISS);
		notaFiscalISS.addItemISS(itemNotaFiscalISS);
	}

	/**
	 * M�todo que executa o parsing da entidade Item.
	 * 
	 * Bloco 0 - Registro 0200
	 * 
	 * Entidade n�o considerada neste exerc�cio.
	 * 
	 * @param linhaCamposEntidade - cont�m as informa��es sobre esta entidade.
	 */
	public void ParseItem(String linhaCamposEntidade) {

		item = new Item();

		String campos[] = getCamposLinhaEntidade(linhaCamposEntidade);

		item.setRegistroItem(campos[1]);
		item.setCodigoItem(campos[2]);
		item.setDescricaoItem(campos[3]);
		item.setTipoItem(campos[4]);
		item.setNcmItem(campos[5]);
	}

	/**
	 * M�todo que executa o parsing da entidade Participante.
	 * 
	 * Bloco 0 - Registro 0150
	 * 
	 * @param linhaCamposEntidade - cont�m as informa��es sobre esta entidade.
	 */
	public void ParseParticipantes(String linhaCamposEntidade) {

		participante = new Participante();

		String campos[] = getCamposLinhaEntidade(linhaCamposEntidade);

		participante.setCodigoParticipante(campos[2]);
		participante.setNome(campos[3]);
		if (campos[5].length() > 0)
			participante.setNi(Long.parseLong(campos[5]));
		else if (campos[6].length() > 0)
			     participante.setNi(Long.parseLong(campos[6]));
		else participante.setNi(0);
		participante.setCodigoMunicipio(campos[8]);
		participante.setEndereco(campos[10]);

		participante.setEFD(efd);
		efd.setParticipante(participante);
	}

	/**
	 * M�todo que: -substitui todas as v�rgulas por ponto. -substitui "????" por
	 * "��" e "??" por "�". -identifica e retorna todos os campos da linhaEntidade.
	 * 
	 * @param linhaCamposEntidade - cont�m as informa��es separadas por "|"
	 */
	private String[] getCamposLinhaEntidade(String linhaCamposEntidade) {

		return linhaCamposEntidade.replace(',', '.').replace("????", "��").replace("??", "�").split("\\|");
	}
}
