package gov.rfb.contagil.sped.efd.parse;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Enumera��o que cont�m os nomes dos m�todos da classe Parse que <br>
 * correspondem ao parse de cada entidade considerada neste exerc�cio.
 * 
 * @author Marcus Cesar Pontes
 *
 */
public enum ParseEntidade {

	/** Nome dos m�todos de parse correspondentes a cada entidade */
	ParseEFD, ParseParticipantes, ParseItens, ParseNotaFiscalICMS, ParseNotaFiscalISS, ParseItemNotaFiscalICMS,
	ParseItemNotaFiscalISS, EncerraParse;

	/** Instancia da Classe que contem os m�todos de parse de todas as Entidades */
	private Parse instancia = Parse.getInstanciaParse();

	/** FQN da Classe Parse */
	private String fqn = "gov.rfb.contagil.sped.efd.parse.Parse";

	public void Parse(String linhaCamposEntidade) {

		try {

			/**
			 * Obt�m o m�todo da Classe Parse cujo nome corresponde a este Enum:
			 * this.toString()
			 */
			Method metodoParse = Class.forName(fqn).getMethod(this.toString(), String.class);
			
			/** Executa o m�todo de parsing da entidade correspondente a este Enum */
			metodoParse.invoke(this.instancia, linhaCamposEntidade);

		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | ClassNotFoundException e) {

			e.printStackTrace();
			
		}

	}

}
