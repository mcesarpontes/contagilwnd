package gov.rfb.contagil.sped.efd.parse;

/**
 * Invoca o Parse das entidades consideradas neste exerc�cio e <br>
 * cadastradas nesta tabela. Em uma importa��o real, talvez <br>
 * n�o seja necess�rio tendo em vista que todos os registros <br>
 * devem ser considerados.
 * 
 * considera o registro "9999"  o �ltimo registro v�lido.
 * 
 * @param essaEntidade  c�digo da entidade
 */

import java.util.HashMap;
import java.util.Map;

public class TabelaParseEntidades {

	private static Map<String, ParseEntidade> tipoEntidade = new HashMap<>();

	static {
		tipoEntidade.put("0000", ParseEntidade.ParseEFD);
	    tipoEntidade.put("0150", ParseEntidade.ParseParticipantes);
		tipoEntidade.put("A100", ParseEntidade.ParseNotaFiscalISS);
		tipoEntidade.put("A170", ParseEntidade.ParseItemNotaFiscalISS);
		tipoEntidade.put("C100", ParseEntidade.ParseNotaFiscalICMS);
		tipoEntidade.put("C170", ParseEntidade.ParseItemNotaFiscalICMS);
		tipoEntidade.put("9999", ParseEntidade.EncerraParse);
	}

	
	/**
	 * Verifica se o c�digo est� cadastrado para o parsing.
	 * 
	 * @param essaEntidade - c�digo da entidade
	 * @param linhaCamposEntidade - linha com os dados da entidade
	 * @return - retorna false quando recebe o �ltimo registro "9999"
	 */
	public boolean Parse(String essaEntidade, String linhaCamposEntidade) {

		if (tipoEntidade.containsKey(essaEntidade)) {
			
			tipoEntidade.get(essaEntidade).Parse(linhaCamposEntidade);
		}

		return (essaEntidade.compareTo("9999") == 0);

	}

}
