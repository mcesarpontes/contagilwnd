package gov.rfb.contagil.sped.efd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.guga.contagil.analitico.MAD;
import org.guga.contagil.model.AFieldDescriptor;
import org.guga.contagil.model.ComparadorGenerico;
import org.guga.contagil.scripting.Data;
import org.guga.contagil.scripting.impl.DataImpl;

import gov.rfb.contagil.commons.tipos.AnGrao;
import gov.rfb.contagil.commons.tipos.ReflexiveType;
import gov.rfb.contagil.commons.utils.Persistencia;
import gov.rfb.contagil.commons.utils.TimeUtils;
/**
 * Informações resumidas sobre uma nota fiscal do ICMS
 * 
 * Bloco C - Registro C100
 * 
 * @author Marcus Cesar Pontes
 */
@MAD(madTipo="NotaICMS",
madNomeBreve="NotaICMS")
@Persistencia.Tabela(value="NotaICMS")
public class NotaFiscalICMS implements java.io.Serializable, AnGrao,ReflexiveType, Comparable<NotaFiscalICMS>,
gov.rfb.contagil.sped.efd.scripting.NotaFiscalICMS{
	
	private static final long serialVersionUID = 1L; 

	@AFieldDescriptor(nomeExterno="202 Participante")
	//@Persistencia.TabelaDominioReferenciada(PARTICIPANTES.class)
	@Persistencia.Coluna
	private String codigoParticipante;
	
	@AFieldDescriptor(nomeExterno="200 Numero Nota")
	@Persistencia.Coluna
	@Persistencia.Chave
	private String numeroNota;
	
	@AFieldDescriptor(nomeExterno="201 Data Emissao")
	@Persistencia.Coluna
	private Date dataEmissao;
	
	@AFieldDescriptor(nomeExterno="203 Valor Nota")
	@Persistencia.Coluna
	private double valorTotalNota;
	
	@AFieldDescriptor(nomeExterno="204 Valor PIS")
	@Persistencia.Coluna
	private double valorPIS;

	@AFieldDescriptor(nomeExterno="205 Valor COFINS")
	@Persistencia.Coluna
	private double valorCOFINS;

	
	@Persistencia.Reciproco()
	private transient EFD efd;
	
	@Persistencia.TabelaReferenciada(ItemICMS.class)
	private List<ItemICMS> itens;
	
	
	/* Getters and Setters */
	
	
	public EFD getEFD() {
		return efd;
	}

	public void setEFD(EFD efd) {
		this.efd = efd;
	}

	public void addItemICMS(ItemICMS item) {
		if (itens == null)
			itens = new ArrayList<>();
		itens.add(item);
	}
	
	public double getValorPIS() {
		return valorPIS;
	}

	public void setValorPIS(double valorPIS) {
		this.valorPIS = valorPIS;
	}

	public double getValorCOFINS() {
		return valorCOFINS;
	}

	public void setValorCOFINS(double valorCOFINS) {
		this.valorCOFINS = valorCOFINS;
	}


	public String getCodigoParticipante() {
		return codigoParticipante;
	}

	public void setCodigoParticipante(String codigoParticipante) {
		this.codigoParticipante = codigoParticipante;
	}
	


	public String getNumeroNota() {
		return numeroNota;
	}

	public void setNumeroNota(String numeroDocFiscal) {
		this.numeroNota = numeroDocFiscal;
	}

	public Data getDataEmissao() {
		if (dataEmissao == null) return null;
		else return new DataImpl(dataEmissao);
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public double getValorTotalNota() {
		return valorTotalNota;
	}

	public void setValorTotalNota(double valorTotalDocFiscal) {
		this.valorTotalNota = valorTotalDocFiscal;
	}

	public List<ItemICMS> getItens() {
		return itens;
	}

	public void setItens(List<ItemICMS> itens) {
		this.itens = itens;
	}

	public int compareTo(NotaFiscalICMS ref) {
		int comp = 0;
		if (this==ref) {
			return comp;
		}

		if (ref == null) {
			return +1;
		}

		comp = ComparadorGenerico.COMPARE(numeroNota, ref.numeroNota);
		if (comp != 0)
			return comp;

		return 0;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash = 37 * (hash + ComparadorGenerico.HASHCODE(numeroNota));

		return 17 + hash;
	}

	@Override
	public boolean equals(Object o) {
		boolean equal = true;
		if (this == o) {
			return equal;
		}

		if (o == null) {
			return !equal;
		}

		if (!(o instanceof NotaFiscalICMS)) {
			return !equal;
		}

		NotaFiscalICMS ref  = (NotaFiscalICMS) o;
		equal = ComparadorGenerico.EQUALS(numeroNota, ref.numeroNota);
		if (!equal)
			return false;

		return equal;
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {

		in.defaultReadObject();
		
		dataEmissao = TimeUtils.fixDST(dataEmissao);
		
		if (itens !=null)
			
			for (ItemICMS itemICMS : itens) itemICMS.setNotaICMS(this);
		
	}	
	
	@Override
	public String toString() {
		
		return "[NotaICMS] = "+"["+this.numeroNota+" "+
		this.codigoParticipante+" "+
		this.valorTotalNota+" "+
		this.dataEmissao+" "+
		this.valorPIS+" "+
		this.valorCOFINS+"]";
	}
}


