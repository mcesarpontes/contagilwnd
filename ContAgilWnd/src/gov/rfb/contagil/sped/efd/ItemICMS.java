package gov.rfb.contagil.sped.efd;

import org.guga.contagil.analitico.MAD;
import org.guga.contagil.model.AFieldDescriptor;
import org.guga.contagil.model.ComparadorGenerico;

import gov.rfb.contagil.commons.tipos.AnGrao;
import gov.rfb.contagil.commons.tipos.ReflexiveType;
import gov.rfb.contagil.commons.utils.Persistencia;
/**
 * Informa��es resumidas sobre os itens de uma nota fiscal do ICMS
 * 
 * Bloco C - Registro C170
 * 
 * @author Marcus Cesar Pontes
 *
 */
 
@MAD(madTipo="ItemICMS",
madNomeBreve="ItemICMS")
@Persistencia.Tabela(value="ItemICMS")
public class ItemICMS implements java.io.Serializable, AnGrao,ReflexiveType, Comparable<ItemICMS>,
gov.rfb.contagil.sped.efd.scripting.ItemICMS{
	
	private static final long serialVersionUID = 1L; 
	
	@AFieldDescriptor(nomeExterno="300 Codigo Item")
	@Persistencia.Coluna
	@Persistencia.Chave	
	private String codigoItem;
	
	@AFieldDescriptor(nomeExterno="301 Numero Item")
	@Persistencia.Coluna
	private String numerolItem; 
	
	@AFieldDescriptor(nomeExterno="302 Descricao")
	@Persistencia.Coluna
	private String descricaoItem; 
	
	@AFieldDescriptor(nomeExterno="303 Valor")
	@Persistencia.Coluna
	private double valorTotalItem;
	
	@AFieldDescriptor(nomeExterno="304 PIS")
	@Persistencia.Coluna
	private double valorPIS;
	
	@AFieldDescriptor(nomeExterno="305 COFINS")
	@Persistencia.Coluna
	private double valorCOFINS;

	
	@Persistencia.Reciproco
	private transient NotaFiscalICMS notaFiscalICMS;

	
	
	/* Getters and Setters */

	public String getNumeroItem() {
		return numerolItem;
	}
	public void setNumeroItem(String sequencialItem) {
		this.numerolItem = sequencialItem;
	}
	public String getCodigoItem() {
		return codigoItem;
	}
	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}
	public String getDescricaoItem() {
		return descricaoItem;
	}
	public void setDescricaoItem(String descicaoComplementar) {
		this.descricaoItem = descicaoComplementar;
	}
	
	public double getValorTotalItem() {
		return valorTotalItem;
	}
	public void setValorTotalItem(double valorTotalItem) {
		this.valorTotalItem = valorTotalItem;
	}
	public double getValorPIS() {
		return valorPIS;
	}
	public void setValorPIS(double valorPIS) {
		this.valorPIS = valorPIS;
	}
	public double getValorCOFINS() {
		return valorCOFINS;
	}
	public void setValorCOFINS(double valorCOFINS) {
		this.valorCOFINS = valorCOFINS;
	}
	
	public void setNotaICMS(NotaFiscalICMS notaICMS) {
		this.notaFiscalICMS=notaICMS;
	}
	
	public NotaFiscalICMS getNotaFiscalICMS() {
		return notaFiscalICMS;
	}
	
	
	public int compareTo(ItemICMS ref) {
		int comp = 0;
		if (this==ref) {
			return comp;
		}

		if (ref == null) {
			return +1;
		}

		comp = ComparadorGenerico.COMPARE(codigoItem, ref.codigoItem);
		if (comp != 0)
			return comp;

		return 0;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash = 37 * (hash + ComparadorGenerico.HASHCODE(codigoItem));

		return 17 + hash;
	}

	@Override
	public boolean equals(Object o) {
		boolean equal = true;
		if (this == o) {
			return equal;
		}

		if (o == null) {
			return !equal;
		}

		if (!(o instanceof ItemICMS)) {
			return !equal;
		}

		ItemICMS ref  = (ItemICMS) o;
		equal = ComparadorGenerico.EQUALS(codigoItem, ref.codigoItem);
		if (!equal)
			return false;

		return equal;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public String toString() {
		
		return "[ItemICMS] = "+"["+" N� nota ICMS "+
		this.getNotaFiscalICMS().getNumeroNota()+" "+
		this.numerolItem+" "+
		this.codigoItem+" "+
		this.descricaoItem+" "+
		this.valorTotalItem+" "+
		this.valorPIS+" "+
		this.valorCOFINS+"]";
	}
	
	
	
}























