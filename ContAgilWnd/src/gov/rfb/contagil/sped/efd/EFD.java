package gov.rfb.contagil.sped.efd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.guga.contagil.analitico.MAD;
import org.guga.contagil.model.AFieldDescriptor;
import org.guga.contagil.model.ComparadorGenerico;
import org.guga.contagil.scripting.Data;
import org.guga.contagil.scripting.impl.DataImpl;

import gov.rfb.contagil.commons.tipos.AnGrao;
import gov.rfb.contagil.commons.tipos.CNPJ;
import gov.rfb.contagil.commons.tipos.DadoIdentificaContribuinte;
import gov.rfb.contagil.commons.tipos.ReflexiveType;
import gov.rfb.contagil.commons.utils.Persistencia;
import gov.rfb.contagil.commons.utils.TimeUtils;
import gov.rfb.contagil.commons.utils.Transformacao;
import gov.rfb.contagil.sped.efd.tabelas.dominio.TabelasDominio.ATIVIDADE;
import gov.rfb.contagil.sped.efd.tabelas.dominio.TabelasDominio.MUNICIPIO;

/**
 * Informações resumidas sobre a EFD
 * 
 * Bloco 0 - Registro 0000
 * 
 * @author Marcus Cesar Pontes
 * 
 */
@MAD(madTipo = "EFD", madNomeBreve = "EFD")
@Persistencia.Tabela(value = "EFD")
public class EFD implements java.io.Serializable, AnGrao, ReflexiveType, Comparable<EFD>,
		gov.rfb.contagil.sped.efd.scripting.EFD, DadoIdentificaContribuinte{

	private static final long serialVersionUID = 1L;

	@Persistencia.Coluna
	@Persistencia.Chave
	@Persistencia.IdentificaContribuinte
	@Transformacao.CodigoIdentificacaoTransformado(CNPJ.class)
	@AFieldDescriptor(nomeExterno = "100 CNPJ")
	private CNPJ cnpj;

	@AFieldDescriptor(nomeExterno = "101 Nome")
	@Persistencia.Coluna
	private String nome;
	
	@AFieldDescriptor(nomeExterno = "102 Data Inicial")
	@Persistencia.Coluna
	private Date dataInicial;
	
	@AFieldDescriptor(nomeExterno = "103 Data Final")
	@Persistencia.Coluna
	private Date dataFinal;
	
	@Persistencia.TabelaDominioReferenciada(ATIVIDADE.class)
	@Persistencia.Coluna
	private int codigoAtividade;
	
	@AFieldDescriptor(nomeExterno = "104 Atividade")
	private transient String nomeAtividade;
		
	@Persistencia.TabelaDominioReferenciada(MUNICIPIO.class)
	@Persistencia.Coluna
	private int codigoMunicipio;
	
	@AFieldDescriptor(nomeExterno = "105 Municipio")
	private transient String nomeMunicipio;
	
	@AFieldDescriptor(nomeExterno = "106 UF")
	@Persistencia.Coluna
	private String unidadeFederacao;
		
	
	@Persistencia.TabelaReferenciada(NotaFiscalICMS.class)
	private List<NotaFiscalICMS> notasFiscaisICMS;

	@Persistencia.TabelaReferenciada(NotaFiscalISS.class)
	private List<NotaFiscalISS> notasFiscaisISS;
	
	@Persistencia.TabelaReferenciada(Participante.class)
	private List<Participante> participantes;

	
	/* Getters and Setters */
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCnpj() {
		return (cnpj == null) ? 0 : cnpj.getCodigo();
	}
	
	public CNPJ getCNPJ() {
		return cnpj;
	}

	public void setCnpj(long cnpj) {
		this.cnpj = (cnpj == 0) ? null : new CNPJ(cnpj);
	}

	public List<NotaFiscalICMS> getNotasFiscaisICMS() {
		return notasFiscaisICMS;
	}
	
//	public List <? extends gov.rfb.contagil.sped.efd.scripting.NotaFiscalICMS> getNotasFiscaisICMS(){
//		return notasFiscaisICMS;	
//	}

	public void setNotasFiscaisICMS(List<NotaFiscalICMS> notasFiscaisICMS) {
		this.notasFiscaisICMS = notasFiscaisICMS;
	}

	public List<NotaFiscalISS> getNotasFiscaisISS() {
		return notasFiscaisISS;
	}

	public void setNotasFiscaisISS(List<NotaFiscalISS> notasFiscaisISS) {
		this.notasFiscaisISS = notasFiscaisISS;
	}

	public void setNotaFiscalICMS(NotaFiscalICMS nota) {
		if (this.notasFiscaisICMS == null)
			notasFiscaisICMS = new ArrayList<>();
		this.notasFiscaisICMS.add(nota);
	}

	public void setNotaFiscalISS(NotaFiscalISS nota) {
		if (notasFiscaisISS == null)
			notasFiscaisISS = new ArrayList<>();
		this.notasFiscaisISS.add(nota);

	}

	public List<Participante> getParticipantes() {
		return participantes;
	}

	public void setParticipantes(List<Participante> participantes) {
		this.participantes = participantes;
	}
		
	public void setParticipante (Participante participante) {
		if (participantes == null)
			participantes = new ArrayList<>();
		this.participantes.add(participante);
		
		
	}

	public void setDataInicio(Date dataInicial) {
		this.dataInicial = dataInicial;
	}
	
	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
	
	public Data getDataInicial() {
		if (dataInicial==null) return null;
		else return new DataImpl(dataInicial);
	}
	
	public Data getDataFinal() {
		if (dataFinal==null) return null;
		else return new DataImpl(dataFinal);
	}
	
	public void setCodigoAtividade (int atividade) {
		this.codigoAtividade=atividade;
	}
	
	public int getCodigoAtividade () {
		return this.codigoAtividade;
	}
	
	public void setNomeAtividade () {
		this.nomeAtividade= getDescricaoAtividade();
	}
	
	public String getNomeAtividade() {
		return nomeAtividade;
	}
	
	public String getDescricaoAtividade() {
		return ATIVIDADE.getDefault().getDescricao(codigoAtividade);
	}

	public void setCodigoMunicipio (int codigoMunicipio) {
		this.codigoMunicipio= codigoMunicipio;
	}

	public int getCodigoMunicipio () {
		return this.codigoMunicipio;
	}
	
	public String getDescricaoMunicipio() {
		return MUNICIPIO.getDefault().getDescricao(codigoMunicipio);
	}
	
	
	public void setNomeMunicipio () {
		this.nomeMunicipio = getDescricaoMunicipio();
	}
	
	public String getNomeMunicipio () {
		return nomeMunicipio;
	}
	
	
	
	public String getUnidadeFederacao() {
		return unidadeFederacao;
	}
	
	public void setUnidadeFederacao(String unidadeFederacao) {
		this.unidadeFederacao = unidadeFederacao;
	}
	
	public int compareTo(EFD ref) {
		int comp = 0;
		if (this == ref) {
			return comp;
		}

		if (ref == null) {
			return +1;
		}

		comp = ComparadorGenerico.COMPARE(nome, ref.nome);
		if (comp != 0)
			return comp;

		return 0;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash = 37 * (hash + ComparadorGenerico.HASHCODE(nome));

		return 17 + hash;
	}

	@Override
	public boolean equals(Object o) {
		boolean equal = true;
		if (this == o) {
			return equal;
		}

		if (o == null) {
			return !equal;
		}

		if (!(o instanceof EFD)) {
			return !equal;
		}

		EFD ref = (EFD) o;
		equal = ComparadorGenerico.EQUALS(nome, ref.nome);
		if (!equal)
			return false;

		return equal;
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {

		in.defaultReadObject();
		
		setNomeAtividade();
		
		setNomeMunicipio();
		
		dataInicial = TimeUtils.fixDST(dataInicial);
	
		dataFinal = TimeUtils.fixDST(dataFinal);

		if (notasFiscaisICMS != null)
			for (NotaFiscalICMS notaFiscalICMS : notasFiscaisICMS)
				notaFiscalICMS.setEFD(this);

		if (notasFiscaisISS != null)
			for (NotaFiscalISS notaFiscalISS : notasFiscaisISS)
				notaFiscalISS.setEFD(this);
		
		if (participantes != null)
			for (Participante participante : participantes)
                 participante.setEFD(this);
		
	}

	@Override
	public String toString() {
		
		System.out.println("[EFD] = " + "[" + this.nome
				+ " " 
				+ this.cnpj +" "
				+ this.codigoAtividade+" "
				+ this.getDescricaoAtividade()+
				" ]");
		
//para debug
//		if (notasFiscaisICMS != null)
//			notasFiscaisICMS.forEach(icms -> System.out.println(icms));
//
//		if (notasFiscaisISS != null)
//			notasFiscaisISS.forEach(iss -> System.out.println(iss));
//		
//		if (participantes != null)
//			participantes.forEach(part -> System.out.println(part));
		
		return "";
	}



	

}
