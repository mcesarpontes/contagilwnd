package gov.rfb.contagil.sped.efd.analitico.base;

import org.guga.contagil.analitico.utils.ReflexiveBase;

import gov.rfb.contagil.sped.efd.dao.EFDList;
/**
 * Classe que faz a vinculação da entidade ItemISS com o MAD
 * 
 * 
 * @author Marcus Cesar Pontes
 *
 */
public class ItemISSBase extends ReflexiveBase {

	 
	public ItemISSBase() {
		super(ItemISSBase.class, "EFD","notasFiscaisISS","itens");

		setObjetoComElementos(new EFDList());
	}
	
	
	
}
