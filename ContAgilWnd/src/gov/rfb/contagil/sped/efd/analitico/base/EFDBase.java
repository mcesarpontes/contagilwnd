package gov.rfb.contagil.sped.efd.analitico.base;

import org.guga.contagil.analitico.utils.ReflexiveBase;

import gov.rfb.contagil.sped.efd.EFD;
import gov.rfb.contagil.sped.efd.dao.EFDList;

/**
 * Classe que faz a vinculação da entidade EFD com o MAD
 * 
 * 
 * @author Marcus Cesar Pontes
 *
 */
 


public class EFDBase extends ReflexiveBase {

	public EFDBase() {
		super(EFD.class, "EFD");

		setObjetoComElementos(new EFDList());
	}

}
