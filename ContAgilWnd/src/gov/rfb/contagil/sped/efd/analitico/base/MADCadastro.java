package gov.rfb.contagil.sped.efd.analitico.base;

import org.guga.contagil.acoes.OpenDocumentsManager;
import org.guga.contagil.acoes.mads.RefModeloAnaliticoDinamico;
import org.guga.contagil.acoes.mads.RefModeloAnaliticoDinamicoBase;

import gov.rfb.contagil.sped.efd.EFD;
import gov.rfb.contagil.sped.efd.ItemICMS;
import gov.rfb.contagil.sped.efd.ItemISS;
import gov.rfb.contagil.sped.efd.NotaFiscalICMS;
import gov.rfb.contagil.sped.efd.NotaFiscalISS;

public class MADCadastro extends RefModeloAnaliticoDinamicoBase {

	
	public MADCadastro(OpenDocumentsManager man) {
		super(man,madBases,null);
	}
	 
	@Override
	public String getNomeConjunto() {
		
		return "Cadastro";
	}
	
	public static final RefModeloAnaliticoDinamico[] madBases = {
			new RefModeloAnaliticoDinamico (
					"Modelo Analitico/EFD",
					"EFD",
					"EFD",
					EFD.class),
			new RefModeloAnaliticoDinamico(
					"Modelo Analitico/ICMS",
					"NotaICMS",
					"NotaICMS",
					NotaFiscalICMS.class),
			new RefModeloAnaliticoDinamico (
					"Modelo Analitico/ISS",
					"NotaISS",
					"NotaISS",
					NotaFiscalISS.class),
			new RefModeloAnaliticoDinamico (
					"Modelo Analitico/ItemICMS",
					"ItemICMS",
					"ItemICMS",
					ItemICMS.class),
			new RefModeloAnaliticoDinamico (
					"Modelo Analitico/ItemISS",
					"ItemISS",
					"ItemISS",
					ItemISS.class)
	};

	
	
	
	
}
