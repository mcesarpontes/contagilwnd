package gov.rfb.contagil.sped.efd.analitico.base;

import org.guga.contagil.analitico.utils.ReflexiveBase;

import gov.rfb.contagil.sped.efd.dao.EFDList;
/**
 * Classe que faz a vinculação da entidade Participantes com o MAD
 * 
 * @author Marcus Cesar Pontes
 *
 */ 
public class ParticipanteBase extends ReflexiveBase {

	public ParticipanteBase() {
		super(ParticipanteBase.class, "EFD","participantes");

		setObjetoComElementos(new EFDList());
	}
	
	
	
	
}
