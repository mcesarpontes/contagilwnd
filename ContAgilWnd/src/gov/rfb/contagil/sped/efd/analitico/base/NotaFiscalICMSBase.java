package gov.rfb.contagil.sped.efd.analitico.base;

import org.guga.contagil.analitico.utils.ReflexiveBase;

import gov.rfb.contagil.sped.efd.NotaFiscalICMS;
import gov.rfb.contagil.sped.efd.dao.EFDList;
/**
 * Classe que faz a vinculação da entidade NotaFiscalICMS com o MAD
 * 
 * 
 * @author Marcus Cesar Pontes
 *
 */
public class NotaFiscalICMSBase extends ReflexiveBase {
 
	
	public NotaFiscalICMSBase() {
		
		super(NotaFiscalICMS.class, "EFD","notasFiscaisICMS");

		setObjetoComElementos(new EFDList());
	}
	
	
}
