package gov.rfb.contagil.sped.efd.analitico.base;

import org.guga.contagil.analitico.utils.ReflexiveBase;

import gov.rfb.contagil.sped.efd.NotaFiscalISS;
import gov.rfb.contagil.sped.efd.dao.EFDList;
/**
 * Classe que faz a vinculação da entidade NotaFiscalISS com o MAD
 * 
 * 
 * @author Marcus Cesar Pontes
 *
 */
public class NotaFiscalISSBase extends ReflexiveBase {
 
public NotaFiscalISSBase() {
		
		super(NotaFiscalISS.class, "EFD","notasFiscaisISS");

		setObjetoComElementos(new EFDList());
	}
	
	
}
