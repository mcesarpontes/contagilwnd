package gov.rfb.contagil.sped.efd.analitico.base;

import org.guga.contagil.analitico.utils.ReflexiveBase;

import gov.rfb.contagil.sped.efd.dao.EFDList;
/**
 * Classe que faz a vinculação da entidade ItemICMS com o MAD
 * 
 * 
 * @author Marcus Cesar Pontes
 *
 */ 
public class ItemICMSBase extends ReflexiveBase {

	public ItemICMSBase() {
		super(ItemICMSBase.class, "EFD","notasFiscaisICMS","itens");

		setObjetoComElementos(new EFDList());
	}
	
	
	
}
