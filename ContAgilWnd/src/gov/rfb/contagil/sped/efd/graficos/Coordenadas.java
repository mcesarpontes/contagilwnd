package gov.rfb.contagil.sped.efd.graficos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import gov.rfb.contagil.sped.efd.Participante;

public class Coordenadas {

	private List<String> X;
	private List<Integer> Y;
	
	private Map<String, Integer> coordenadas;
	
	
	
	/**
	 * Converte informa��es sobre Unidades da Federa��o em coordenadas (x,y)
	 * 
	 * @param participantes - Informa��o sobre os participantes
	 */
	public Coordenadas (List<Participante> participantes){

		/* Tendo em vista que algum c�digo de munic�pio pode vir com erro,    */
		/* retira da Lista os participantes com o nome do munic�pio inv�lido. */
		/* Seleciona apenas os participantes com CNPJ                         */
		
		List<Participante> listaParticipante = participantes.parallelStream()
		.filter(p->p.isCnpj())		
		.filter(p -> !"<N/D>".equals(p.getNomeUF())).collect(Collectors.toList());
		
		coordenadas = new HashMap<>();

		for (Participante p : listaParticipante) {

			/* Para ficar mais leg�vel */
			String nomeUF = p.getNomeUF();

			if (coordenadas.containsKey(nomeUF)) {
				coordenadas.replace(nomeUF, (coordenadas.get(nomeUF) + 1));
			} else {
				coordenadas.put(nomeUF, 1);
			}
		}
		
		X = coordenadas.keySet().stream().collect(Collectors.toList());
		Y = coordenadas.values().stream().collect(Collectors.toList());
		
		
	}

	
	/**
	 * @return coordenada x
	 */
	public List<String> getX() {
		return X;
	}
	/**
	 * @return coordenada y
	 */
	public List<Integer> getY() {
		return Y;
	}
	
}
