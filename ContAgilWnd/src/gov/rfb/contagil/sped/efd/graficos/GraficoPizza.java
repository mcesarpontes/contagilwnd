package gov.rfb.contagil.sped.efd.graficos;

import org.guga.contagil.scripting.utils.Bridge;
import org.guga.contagil.scripting.utils.Grafico;
import org.guga.contagil.scripting.utils.Graficos;
import org.jfree.chart.ChartPanel;

/**
 * Cria um gr�fico pizza com base no par�metro coordenadas
 *  
 * @author Marcus Cesar Pontes
 *
 */
public class GraficoPizza {

	public ChartPanel build(Coordenadas coordenadas ) {

		Grafico graficoPizza = new Graficos().novoGraficoPizza();
		
		graficoPizza.addValores(coordenadas.getX(),coordenadas.getY());
		
		return new ChartPanel (Bridge.render(graficoPizza));
		
	}

}	