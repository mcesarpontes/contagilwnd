package gov.rfb.contagil.sped.efd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.guga.contagil.analitico.MAD;
import org.guga.contagil.model.AFieldDescriptor;
import org.guga.contagil.model.ComparadorGenerico;
import org.guga.contagil.scripting.Data;
import org.guga.contagil.scripting.impl.DataImpl;

import gov.rfb.contagil.commons.tipos.AnGrao;
import gov.rfb.contagil.commons.tipos.ReflexiveType;
import gov.rfb.contagil.commons.utils.Persistencia;
import gov.rfb.contagil.commons.utils.TimeUtils;
/**
 * Informações resumidas sobre uma nota fiscal do ICMS
 *  
 * Bloco A - Registro A100
 * 
 * @author Marcus Cesar Pontes
 */
@MAD(madTipo="NotaISS",
madNomeBreve="NotaISS")
@Persistencia.Tabela(value="NotaISS")
public class NotaFiscalISS implements java.io.Serializable, AnGrao,ReflexiveType, Comparable<NotaFiscalISS>,
gov.rfb.contagil.sped.efd.scripting.NotaFiscalISS{
	
	private static final long serialVersionUID = 1L;

	@AFieldDescriptor(nomeExterno="401 Participante")
	@Persistencia.Coluna
	private String codigoParticipante;
	
	@AFieldDescriptor(nomeExterno="400 Numero Nota")
	@Persistencia.Coluna
	@Persistencia.Chave
	private String numeroNota;
	
	@AFieldDescriptor(nomeExterno="402 Data Emissao")
	@Persistencia.Coluna
	private Date dataEmissao;
	
	@AFieldDescriptor(nomeExterno="403 Valor Nota")
	@Persistencia.Coluna
	private double valorTotalNota;
	
	@AFieldDescriptor(nomeExterno="404 Valor PIS")
	@Persistencia.Coluna
	private double valorPIS;
	
	@AFieldDescriptor(nomeExterno="405 Valor COFINS")
	@Persistencia.Coluna
	private double valorCofins;

	
	@Persistencia.Reciproco()
	private EFD efd;
	
	@Persistencia.TabelaReferenciada(ItemISS.class)
	private List<ItemISS> itens;
	
	
	
	/* Getters and Setters */
		
	public EFD getEFD() {
		return efd;
	}

	public void setEFD(EFD efd) {
		this.efd = efd;
	}

	public void addItemISS(ItemISS item) {
		if (itens == null)
			itens = new ArrayList<>();
		itens.add(item);
	}

	public String getCodigoParticipante() {
		return codigoParticipante;
	}

	public void setCodigoParticipante(String codigoParticipanteNotaISS) {
		this.codigoParticipante = codigoParticipanteNotaISS;
	}

	public String getNumeroNota() {
		return numeroNota;
	}

	public void setNumeroNota(String numeroNotaISS) {
		this.numeroNota = numeroNotaISS;
	}


	public Data getDataEmissao() {
		if (dataEmissao==null) return null;
		else return new DataImpl(dataEmissao);
	}

	public void setDataEmissao(Date dataEmissaoNotaISS) {
		this.dataEmissao = dataEmissaoNotaISS;
	}

	public double getValorTotalNota() {
		return valorTotalNota;
	}

	public void setValorTotalNota(double valorTotalNotaISS) {
		this.valorTotalNota = valorTotalNotaISS;
	}


	public double getValorPIS() {
		return valorPIS;
	}

	public void setValorPIS(double valorPisNotaISS) {
		this.valorPIS = valorPisNotaISS;
	}

	public double getValorCOFINS() {
		return valorCofins;
	}

	public void setValorCOFINS(double valorCofinsNotaISS) {
		this.valorCofins = valorCofinsNotaISS;
	}


	public List<ItemISS> getItens() {
		return itens;
	}

	public void setItens(List<ItemISS> itens) {
		this.itens = itens;
	}
	
	public int compareTo(NotaFiscalISS ref) {
		int comp = 0;
		if (this==ref) {
			return comp;
		}

		if (ref == null) {
			return +1;
		}

		comp = ComparadorGenerico.COMPARE(numeroNota, ref.numeroNota);
		if (comp != 0)
			return comp;

		return 0;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash = 37 * (hash + ComparadorGenerico.HASHCODE(numeroNota));

		return 17 + hash;
	}

	@Override
	public boolean equals(Object o) {
		boolean equal = true;
		if (this == o) {
			return equal;
		}

		if (o == null) {
			return !equal;
		}

		if (!(o instanceof NotaFiscalISS)) {
			return !equal;
		}

		NotaFiscalISS ref  = (NotaFiscalISS) o;
		equal = ComparadorGenerico.EQUALS(numeroNota, ref.numeroNota);
		if (!equal)
			return false;

		return equal;
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {

		in.defaultReadObject();
		
		this.dataEmissao = TimeUtils.fixDST(dataEmissao);
		
		if (itens != null)
			
			for (ItemISS itemISS : itens) itemISS.setNotaFiscalISS(this);
			
	}
	
	@Override
	public String toString() {
		
		return "[NotaISS] = "+"["+this.numeroNota+" "+
		this.codigoParticipante+" "+		
		this.valorTotalNota+" "+
		this.dataEmissao.toString()+" "+
		this.valorPIS+" "+
		this.valorCofins+"]";
	}
	
}
