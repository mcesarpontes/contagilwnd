package gov.rfb.contagil.sped.efd.imp;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import gov.rfb.contagil.sped.efd.parse.TabelaParseEntidades;
/**
 * Classe que importa o arquivo texto correspondente � EFD e o passa <br>
 * para os procedimentos de parsing
 *
 * @author Marcus Cesar Pontes
 *
 */
public class ImportaEFD {

	public void Importa() throws FileNotFoundException {


		/** obtem o caminho do arquivo EFD.txt que se deseja importar */
		String arquivoEFD = getPathArquivoEFD();
		
		/** utiliza o Scanner para percorrer as linhas do arquivo */
		Scanner scanner = new Scanner(new FileReader(arquivoEFD));

		/** inicializa tabela que cont�m a relacao das entidades consideradas neste teste */
		TabelaParseEntidades parse = new TabelaParseEntidades();

		/** inicializa vari�vel indicadora do �ltimo registro considerado neste exerc�cio*/
		boolean ultimoRegistro = false;
	
		while ((!ultimoRegistro) && scanner.hasNextLine()) {

			/** Obt�m uma linha do arquivo */
			String linhaCamposEntidade = scanner.nextLine();
			
			/** Obt�m o n�mero do registro/entidade */
			String essaEntidade = linhaCamposEntidade.substring(1, 5);

			/** executa o parsing dessa entidade */
			ultimoRegistro = parse.Parse(essaEntidade, linhaCamposEntidade);
		}

		scanner.close();

		
	}

	
	
	
	
	private String getPathArquivoEFD() {

		JFileChooser chooser = new JFileChooser();

		FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF Images", "txt");
		
		chooser.setDialogTitle("Selecione arquivo EFD.txt para parsing");
	
		chooser.setFileFilter(filter);

		int returnVal = chooser.showOpenDialog(null);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("O Arquivo selecionado --> " + chooser.getSelectedFile().getName());
		}

		return chooser.getSelectedFile().getAbsolutePath();

	}

}
