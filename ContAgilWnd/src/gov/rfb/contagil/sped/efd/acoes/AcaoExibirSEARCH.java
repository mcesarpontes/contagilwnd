package gov.rfb.contagil.sped.efd.acoes;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.List;

import org.guga.contagil.acoes.AcaoAbstract;
import org.guga.contagil.acoes.OpenDocumentsManager;
import org.guga.contagil.gui.SimpleSearch;

import gov.rfb.contagil.sped.efd.EFD;
import gov.rfb.contagil.sped.efd.Participante;
import gov.rfb.contagil.sped.efd.dao.EFDList;

public class AcaoExibirSEARCH extends AcaoAbstract {

	public static final String cmd = "SEARCH Participantes";

	public static final String btn = "btn_Janela";

	/**
	 * Construtor sem par�metros.
	 */
	public AcaoExibirSEARCH() {
		super(cmd, btn);
	}

	/**
	 * Construtor com indica��o de um ID interno.
	 */
	public AcaoExibirSEARCH(String nomeInterno) {
		super(cmd, btn, nomeInterno);
	}

	@Override
	public void run(Window wnd, OpenDocumentsManager man, ActionEvent event) {

		exibirJanelaSEARCH();

	}

	/**
	 * teste
	 */
	public void exibirJanelaSEARCH() {

		SimpleSearch<Participante> janela = new SimpleSearch<>(Participante.class, null);

		List<EFD> dadosEFD = new EFDList().getDados();

		janela.setElementsList(dadosEFD.get(0).getParticipantes());

		janela.build();

	}
}
