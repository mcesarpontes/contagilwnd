package gov.rfb.contagil.sped.efd.acoes;

import java.awt.Window;
import java.awt.event.ActionEvent;

import org.guga.contagil.acoes.AcaoAbstract;
import org.guga.contagil.acoes.OpenDocumentsManager;
import org.guga.contagil.projeto.TipoProjeto;

import gov.rfb.contagil.sped.efd.janelas.MainPanel;

 
/**
 * 
 * Exibe um Resumo da EFD importada
 * 
 * @author Marcus Cesar Pontes
 *
 */
public class AcaoExibirResumo extends AcaoAbstract {

	public static final String cmd = "Resumo EFD";

	public static final String btn = "btn_Resumo";

	/**
	 * Construtor sem par�metros.
	 */
	public AcaoExibirResumo() {
		super(cmd, btn);
	}

	/**
	 * Construtor com indica��o de um ID interno.
	 */
	public AcaoExibirResumo(String nomeInterno) {
		super(cmd, btn, nomeInterno);
	}

	/**
	 * Este m�todo � executado quando o usu�rio seleciona "Resumo EFD"
	 */
	public void run(Window wnd, OpenDocumentsManager man, ActionEvent event) {
		
		new MainPanel(null).build();
		

	}

	/**
	 * Atrav�s desta fun��o podemos habilitar ou desabilitar esta a��o conforme a presen�a
	 * de algum perfil, ou conforme algum tipo de projeto, ou conforme a presen�a ou aus�ncia
	 * de determinados tipos de dados 'abertos' na janela principal da aplica��o.
	 */
	@Override
	public boolean isEnabled(OpenDocumentsManager man, int flags, TipoProjeto tipo) {
		//JOptionPane.showMessageDialog(null, "isEnabled() - EFD");
		// Por comodidade, vamos sempre habilitar esta a��o
		return true;
		
	}
}
