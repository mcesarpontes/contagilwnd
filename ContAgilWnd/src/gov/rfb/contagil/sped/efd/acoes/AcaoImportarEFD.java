package gov.rfb.contagil.sped.efd.acoes;

import java.awt.Window;
import java.awt.event.ActionEvent;

import org.guga.contagil.acoes.AcaoAbstract;
import org.guga.contagil.acoes.OpenDocumentsManager;
import org.guga.contagil.projeto.TipoProjeto;

import gov.rfb.contagil.commons.gui.WaitProcedureAdapter;
import gov.rfb.contagil.commons.gui.WaitWindow;
import gov.rfb.contagil.commons.utils.GuiUtils;
import gov.rfb.contagil.sped.efd.imp.ImportaEFD;
 
/**
 * 
 * Executa a a��o de Iimportar uma EFD
 * 
 * 
 * @author Marcus Cesar
 *
 */
public class AcaoImportarEFD extends AcaoAbstract {

	public static final String cmd = "Importar EFD";

	public static final String btn = "btn_EFD";

	/**
	 * Construtor sem par�metros.
	 */
	public AcaoImportarEFD() {
		super(cmd, btn);
	}

	/**
	 * Construtor com indica��o de um ID interno.
	 */
	public AcaoImportarEFD(String nomeInterno) {
		super(cmd, btn, nomeInterno);
	}

	/**
	 * Este m�todo � executado quando o usu�rio seleciona "Importar EFD"
	 */
	public void run(Window wnd, OpenDocumentsManager man, ActionEvent event) {
		
		//if (!GuiUtils.showConfirm("Deseja importar EFD ?", "Aten��o", wnd))
		//	return;
		
		WaitWindow.runProcedure(wnd, new WaitProcedureAdapter() {
			
											@Override
											public String getPrompt() {
												return "Aguarde enquanto criamos a esrtrutura para a EFD";
											}
								
											@Override
											public void run(WaitWindow w) throws Exception {
												importarEFD();
											}
										});
		
		GuiUtils.showMsg("Opera��o conclu�da!");
	}
	
	/**
	 * Atrav�s desta fun��o podemos habilitar ou desabilitar esta a��o conforme a presen�a
	 * de algum perfil, ou conforme algum tipo de projeto, ou conforme a presen�a ou aus�ncia
	 * de determinados tipos de dados 'abertos' na janela principal da aplica��o.
	 */
	@Override
	public boolean isEnabled(OpenDocumentsManager man, int flags, TipoProjeto tipo) {
		//JOptionPane.showMessageDialog(null, "isEnabled() - EFD");
		// Por comodidade, vamos sempre habilitar esta a��o
		return true;
		
	}
	
	/**
	 * M�todo que importa a EFD
	 */
	private static void importarEFD() throws Exception {
		
		new ImportaEFD().Importa();
		
		
	}
}
