package gov.rfb.contagil.sped.efd.acoes;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.List;

import org.guga.contagil.acoes.AcaoAbstract;
import org.guga.contagil.acoes.OpenDocumentsManager;
import org.guga.contagil.gui.SimpleCRUD;

import gov.rfb.contagil.sped.efd.EFD;
import gov.rfb.contagil.sped.efd.NotaFiscalICMS;
import gov.rfb.contagil.sped.efd.dao.EFDList;

public class AcaoExibirCRUD extends AcaoAbstract {

	public static final String cmd = "CRUD ICMS";

	public static final String btn = "btn_Janela";
 
	/**
	 * Construtor sem par�metros.
	 */
	public AcaoExibirCRUD() {
		super(cmd, btn);
	}

	/**
	 * Construtor com indica��o de um ID interno.
	 */
	public AcaoExibirCRUD(String nomeInterno) {
		super(cmd, btn, nomeInterno);
	}

	@Override
	public void run(Window wnd, OpenDocumentsManager man, ActionEvent event) {
		exibirJanelaCRUD();

	}

	
	
	/**
	 * teste
	 */
	public void exibirJanelaCRUD()  {

		SimpleCRUD<NotaFiscalICMS> janela = new SimpleCRUD<>(NotaFiscalICMS.class, null);
        
        List<EFD> dados = new EFDList().getDados();
		
        janela.setElementsList(dados.get(0).getNotasFiscaisICMS());
		
        janela.build();
		

	}
}
