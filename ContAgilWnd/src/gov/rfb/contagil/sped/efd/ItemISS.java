package gov.rfb.contagil.sped.efd;

import org.guga.contagil.analitico.MAD;
import org.guga.contagil.model.AFieldDescriptor;
import org.guga.contagil.model.ComparadorGenerico;

import gov.rfb.contagil.commons.tipos.AnGrao;
import gov.rfb.contagil.commons.tipos.ReflexiveType;
import gov.rfb.contagil.commons.utils.Persistencia;
/**
 * Informa��es resumidas sobre os itens de uma nota fiscal de ISS
 * 
 * Bloco A - Registro A170
 * 
 * @author Marcus Cesar Pontes
 *
 */
 
@MAD(madTipo="ItemISS",
madNomeBreve="ItemISS")
@Persistencia.Tabela(value="ItemISS")
public class ItemISS implements java.io.Serializable, AnGrao,ReflexiveType, Comparable<ItemISS>,
gov.rfb.contagil.sped.efd.scripting.ItemISS{
	
	private static final long serialVersionUID = 1L;
	
	@AFieldDescriptor(nomeExterno="500 Codigo Item")
	@Persistencia.Coluna
	@Persistencia.Chave	
	private String codigoItem;
	
	@AFieldDescriptor(nomeExterno="501 Numero Item")
	@Persistencia.Coluna
	private String numeroItem;
	
	@AFieldDescriptor(nomeExterno="502 Descricao")
	@Persistencia.Coluna
	private String descricaolItem;
	
	@AFieldDescriptor(nomeExterno="503 Valor")
	@Persistencia.Coluna
	private double valorItem;
	
	@AFieldDescriptor(nomeExterno="504 PIS")
	@Persistencia.Coluna
	private double valorPIS;
	
	@AFieldDescriptor(nomeExterno="505 COFINS")
	@Persistencia.Coluna
	private double valorCOFINS;

	@Persistencia.Reciproco
	private transient NotaFiscalISS notaFiscalISS;
	
	
	/* Getters and Setters */

	public NotaFiscalISS getNotaFiscalISS() {
		return notaFiscalISS;
	}
	
	public void setNotaFiscalISS(NotaFiscalISS notaFiscalISS) {
		this.notaFiscalISS = notaFiscalISS;
	}
	
	public String getNumeroItem() {
		return numeroItem;
	}

	public void setNumeroItem(String numeroItem) {
		this.numeroItem = numeroItem;
	}

	public String getCodigoItem() {
		return codigoItem;
	}

	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}

	public String getDescricaoItem() {
		return descricaolItem;
	}

	public void setDescricaoItem(String descricaoComplItem) {
		this.descricaolItem = descricaoComplItem;
	}

	public double getValorTotalItem() {
		return valorItem;
	}

	public void setValorTotalItem(double valorItem) {
		this.valorItem = valorItem;
	}


	public double getValorPIS() {
		return valorPIS;
	}

	public void setValorPIS(double valorPIS) {
		this.valorPIS = valorPIS;
	}


	public double getValorCOFINS() {
		return valorCOFINS;
	}

	public void setValorCOFINS(double valorCOFINS) {
		this.valorCOFINS = valorCOFINS;
	}


	public int compareTo(ItemISS ref) {
		int comp = 0;
		if (this==ref) {
			return comp;
		}

		if (ref == null) {
			return +1;
		}

		comp = ComparadorGenerico.COMPARE(codigoItem, ref.codigoItem);
		if (comp != 0)
			return comp;

		return 0;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash = 37 * (hash + ComparadorGenerico.HASHCODE(codigoItem));

		return 17 + hash;
	}

	@Override
	public boolean equals(Object o) {
		boolean equal = true;
		if (this == o) {
			return equal;
		}

		if (o == null) {
			return !equal;
		}

		if (!(o instanceof ItemISS)) {
			return !equal;
		}

		ItemISS ref  = (ItemISS) o;
		equal = ComparadorGenerico.EQUALS(codigoItem, ref.codigoItem);
		if (!equal)
			return false;

		return equal;
	}
	
	@Override
	public String toString() {
		
		return "[ItemISS] = "+"["+" N� nota "+
		this.notaFiscalISS.getNumeroNota()+" "+
		this.codigoItem+" "+
		this.numeroItem+" "+
		this.valorItem+" "+
		this.descricaolItem+" "+
		this.valorPIS+" "+
		this.valorCOFINS+"]";
	}
	
	
	
}
