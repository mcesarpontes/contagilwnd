package gov.rfb.contagil.sped.efd.scripting;

import org.guga.contagil.scripting.Data;

import gov.rfb.contagil.commons.tipos.ScriptingObject;

public interface NotaFiscalISS extends ScriptingObject {

	/**
	 * @return c�digo do Participante da Nota Fiscal do ISS
	 */
	public String getCodigoParticipante();

	/**
	 * @return n�mero da Nota Fiscal do ISS
	 */
	public String getNumeroNota();

	/**
	 * @return valor da Nota Fiscal do ISS
	 */
	public double getValorTotalNota();

	/**
	 * @return valor do PIS da Nota Fiscal do ISS
	 */
	public double getValorPIS();

	 /** 
	 * @return valor da COFINS da Nota Fiscal do ISS
	 */
	public double getValorCOFINS();

	/**
	 * @return data de emiss�o da Nota Fiscal do ISS
	 */
	public Data getDataEmissao();
}
