package gov.rfb.contagil.sped.efd.scripting;

import java.util.List;

import gov.rfb.contagil.commons.tipos.ScriptingObject;
import gov.rfb.contagil.sped.efd.EFD;
import gov.rfb.contagil.sped.efd.Participante;
import gov.rfb.contagil.sped.efd.dao.EFDList;

public class EFDScript implements ScriptingObject {

	
     static List<EFD> dados = new EFDList().getDados();
	
	
	/**
	 * @return Retorna a rela��o de EFD existentes no projeto atual.
	 */
	public List<EFD> getEFDScript() {
		 return  dados;                           
	}

	/**
	 * @return Retorna a rela��o de NotasFiscaisICMS  existentes no projeto atual.
	 */
	public List<? extends NotaFiscalICMS> getNotasFiscaisICMS(){
	
    return dados.get(0).getNotasFiscaisICMS();
	}
	
	
	/**
	 * @return Retorna a rela��o de NotasFiscaisICMS  existentes no projeto atual.
	 */
	public List<? extends NotaFiscalISS> getNotasFiscaisISS(){
	
    return dados.get(0).getNotasFiscaisISS();
	}
	
	
	/**
	 * @return Retorna a rela��o de Participantes no projeto atual.
	 */
	public List<? extends Participante> getParticipantes(){
		
	    return dados.get(0).getParticipantes();
	}
	
}
