package gov.rfb.contagil.sped.efd.scripting;

import java.util.List;

import org.guga.contagil.scripting.Data;

import gov.rfb.contagil.commons.tipos.CNPJ;
import gov.rfb.contagil.commons.tipos.ScriptingObject;
import gov.rfb.contagil.sped.efd.NotaFiscalICMS;
import gov.rfb.contagil.sped.efd.NotaFiscalISS;
import gov.rfb.contagil.sped.efd.Participante;

public interface EFD extends ScriptingObject {

	/**
	 * @return nome do contribuinte
	 */
	public String getNome();

	/**
	 * @return CNPJ do contribuinte
	 */
	public CNPJ getCNPJ();

	/**
	 * 
	 * @return Data Inicial da EFD
	 */
	public Data getDataInicial();

	/**
	 * 
	 * @return Data Final da EFD
	 */
	public Data getDataFinal();

	/**
	 * @return lista de Notas Fiscais do ICMS
	 */
	public List<? extends NotaFiscalICMS> getNotasFiscaisICMS();

	/**
	 * @return lista de Notas Fiscais do ISS
	 */
	public List<? extends NotaFiscalISS> getNotasFiscaisISS();

	/**
	 * @return lista de Participantes da EFD
	 */
	public List<? extends Participante> getParticipantes();

}
