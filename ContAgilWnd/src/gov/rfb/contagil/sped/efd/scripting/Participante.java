package gov.rfb.contagil.sped.efd.scripting;

import gov.rfb.contagil.commons.tipos.CNPJ_CPF;
import gov.rfb.contagil.commons.tipos.ScriptingObject;

public interface Participante extends ScriptingObject {

	/**
	 * @return c�digo do participante
	 */
	public String getCodigoParticipante();

	/**
	 * @return nome do participante
	 */
	public String getNome();

	/**
	 * @return CPF/CNPJ do participante
	 */
	public CNPJ_CPF getNI();

	/**
	 * @return endere�o do participante
	 */
	public String getEndereco();

	/**
	 * @return c�digo do munic�pio do participante
	 */
	public String getCodigoMunicipio();

}
