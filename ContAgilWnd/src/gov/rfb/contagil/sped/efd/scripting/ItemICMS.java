package gov.rfb.contagil.sped.efd.scripting;

import gov.rfb.contagil.commons.tipos.ScriptingObject;

public interface ItemICMS extends ScriptingObject {

	/**
	 * @return c�digo do item da Nota Fiscal do ICMS
	 */
	public String getCodigoItem();

	/**
	 * @return n�mero do item da Nota Fiscal do ICMS
	 */
	public String getNumeroItem();

	/**
	 * @return descri��o do item da Nota Fiscal do ICMS
	 */
	public String getDescricaoItem();

	/**
	 * @return valor do item da Nota Fiscal do ICMS
	 */
	public double getValorTotalItem();

	/**
	 * @return valor do PIS referente ao item da Nota Fiscal do ICMS
	 */
	public double getValorPIS();

	/**
	 * @return valor da COFINS referente ao item da Nota Fiscal do ICMS
	 */
	public double getValorCOFINS();

}
