package gov.rfb.contagil.sped.efd.scripting;

import gov.rfb.contagil.commons.tipos.ScriptingObject;

public interface ItemISS extends ScriptingObject {
	
	/**
	 * @return c�digo do item da Nota Fiscal do ISS
	 */
	public String getCodigoItem();

	/**
	 * @return n�mero do item da Nota Fiscal do ISS
	 */
	public String getNumeroItem();

	/**
	 * @return descri��o do item da Nota Fiscal do ISS
	 */
	public String getDescricaoItem();

	/**
	 * @return valor do item da Nota Fiscal do ISS
	 */
	public double getValorTotalItem();

	/**
	 * @return valor do PIS referente ao item da Nota Fiscal do ISS
	 */
	public double getValorPIS();

	/**
	 * @return valor da COFINS referente ao item da Nota Fiscal do ISS
	 */
	public double getValorCOFINS();
}
