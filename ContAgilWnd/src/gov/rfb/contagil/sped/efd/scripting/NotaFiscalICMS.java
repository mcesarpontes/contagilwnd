package gov.rfb.contagil.sped.efd.scripting;

import org.guga.contagil.scripting.Data;

import gov.rfb.contagil.commons.tipos.ScriptingObject;

public interface NotaFiscalICMS extends ScriptingObject {
	/**
	 * @return c�digo do Participante da Nota Fiscal do ICMS
	 */
	public String getCodigoParticipante();

	/**
	 * @return n�mero da Nota Fiscal do ICMS
	 */
	public String getNumeroNota();

	/**
	 * @return valor da Nota Fiscal do ICMS
	 */
	public double getValorTotalNota();

	/**
	 * @return valor do PIS da Nota Fiscal do ICMS
	 */
	public double getValorPIS();

	 /** 
	 * @return valor da COFINS da Nota Fiscal do ICMS
	 */
	public double getValorCOFINS();

	/**
	 * @return data de emiss�o da Nota Fiscal do ICMS
	 */
	public Data getDataEmissao();
}
