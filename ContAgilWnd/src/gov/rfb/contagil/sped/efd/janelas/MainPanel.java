package gov.rfb.contagil.sped.efd.janelas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import org.guga.contagil.gui.DefaultDialogOrFrame;

import gov.rfb.contagil.sped.efd.EFD;
import gov.rfb.contagil.sped.efd.dao.EFDList;
/**
 * Panel principal do Menu "RESUMO EFD" que composto de duas partes:
 * 
 * Panel superior com uma tabela pagin�vel apresentando os dados dos participantes da EFD.
 * 
 * Panel inferior, separado por um SplitPane, contendo TabbedPane que exibe dois graficos 
 * e uma tabela com participantes.
 *  
 * @author Marcus Cesar Pontes
 *
 */
public class MainPanel extends DefaultDialogOrFrame {

	private JSplitPane splitPane;

	public MainPanel(Window owner) {
		super(owner);
	}

	@Override
	protected void build(JPanel main) {

		main.setPreferredSize(new Dimension(850, 800));
		main.setOpaque(true);
		SetBtnMainPanel();
		
		EFD dados = (EFD) new EFDList().getDados().get(0);

		// Simulador
		 //EFD dados = new DAO().read();

		/** Prepara Panel Superior */
		JPanel superiorPanel = new JPanel();

		superiorPanel.setLayout(new BoxLayout(superiorPanel, BoxLayout.PAGE_AXIS));
		superiorPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));

		/* Adiciona ao superiorPanel um formul�rio pagin�vel */
		superiorPanel.add(new FormPanel().build(dados));
		
		
		/** Prepara Panel Inferior */
		JPanel inferior = new JPanel();

		inferior.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		/* Adiciona ao inferiorPanel um TabbedPane  */  
		inferior.add(new TabbedPanel().build(dados));

		/* insere um JSCrollPane no inferiorPanel */
		JScrollPane inferiorPanel = new JScrollPane(inferior);

		/* Insere Panels Superior e Inferior em um SplitPane */
		splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, superiorPanel, inferiorPanel);
		splitPane.setPreferredSize(new Dimension(900, 550));
		splitPane.setEnabled(false);
		
		/* Insere splitPanel na janela principal */
		main.add(splitPane, BorderLayout.NORTH);

	}

	
	/**
	 * Exibe mensagem de Help
	 */
	@Override
	public String getHelp() {
		return " Janela que, na parte superior, apresenta um formul�rio com os dados b�sicos "
				+ "dos Participantes de uma EFD "
				+ "e que na parte inferior exibe dois tipos de gr�ficos e uma tabela tamb�m "
				+ "com os dados dos Participantes, por�m com outro formato.\n"
				+ "O Bot�o \"Split on/Off\"  habilita/desabilita o movimento da barra que divide as telas."
				+ "\nDesmarque \"Ajuda\" para ocultar esta caixa de mensagens";
	}

	@Override
	protected void onOk() {

	}

	/**
	 * Gerencia o bot�o que habilita/desbilita o splitPanes
	 */
	public void actionPerformed(ActionEvent evento) {

//		if (evento.getActionCommand().contentEquals("Split On")) {
//			this.btnOk.setText("Split Off");
//			splitPane.setEnabled(true);
//		} else if (evento.getActionCommand().contentEquals("Split Off")) {
//			this.btnOk.setText("Split On");
//			splitPane.setEnabled(false);
//			}
		
		if ((evento.getActionCommand().contentEquals("Split On"))|
		   (evento.getActionCommand().contentEquals("Split Off"))){
			
			splitPane.setEnabled(!splitPane.isEnabled());
			
			this.btnOk.setText(splitPane.isEnabled() ? "Split Off": "Split On");
						
		}
		
		
	}

	public void SetBtnMainPanel () {
		this.btnOk.setText("Split On");
		this.btnCancel.setText("Sair");
		this.btnCancel.setActionCommand("Voltar");
	}
	
}
