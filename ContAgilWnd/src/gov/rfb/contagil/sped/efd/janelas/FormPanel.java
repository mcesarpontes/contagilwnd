package gov.rfb.contagil.sped.efd.janelas;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import org.guga.contagil.gui.PaginationBar;
import org.guga.contagil.gui.PaginationBar.PaginationListener;
import org.guga.contagil.model.CommonObjectComponent;

import gov.rfb.contagil.sped.efd.EFD;
import gov.rfb.contagil.sped.efd.Participante;
/**
 * Cria um JPanel com um CommonObjectComponent associado a um
 * PaginationBar que pagina os dados dos perticipantes de uma EFD
 * 
 * @author Marcus Cesar Pontes
 *
 */
public class FormPanel implements PaginationListener {

	private CommonObjectComponent formulario;

	private PaginationBar paginationBar;
	
	
	public JPanel build(EFD dados) {
	
		// Cria o componente PaginatioBar
		paginationBar = new PaginationBar();

		
	    // Ordena os participantes pelo nome da UF e depois pelo nome do participante.
		// Retira os participantes com nome da UF inv�lido.
		List<Participante> valoresPaginaveis = dados.getParticipantes().stream()
				.filter(p->p.isCnpj())
				.filter(p -> !"<N/D>".equals(p.getNomeUF()))
				.sorted(Comparator.comparing(Participante::getNomeUF)
				.thenComparing(Comparator.comparing(Participante::getNome)))
				.collect(Collectors.toList());
		
		// Insere dados pagin�veis
		paginationBar.setValoresPaginaveis(valoresPaginaveis);

		// Inicializa Listener
		paginationBar.setListener(this);

		// Build PaginationBar
		paginationBar.build();
		
		// Configura o componente formul�rio
		formulario = new CommonObjectComponent();

		// R�tulo do formul�rio
		formulario.setLabel("Participantes EFD");

		// Inst�ncia que cont�m os dados que ser�o apresentados
		setObjectFormulario (valoresPaginaveis.get(0));

		// Campos edit�veis
		formulario.setEditavel(false);

		// Com ScrollBar
		formulario.setScrollable(true);

		// Constr�i o componente
		formulario.build();
		
		// Cria Panel com formulario e paginationBar
		JPanel panel = new JPanel();
		
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
		panel.add(formulario.getComponent());
		panel.add(paginationBar.getComponent());

		// Retorna um JPanel contendo o CommonObjectComponent e o correspondente PaginationBar
		return panel;
	}
	
	
	/**
	 * @param participante dados do participante selecionado
	 */
	private void setObjectFormulario(Participante participante) {
		formulario.setObject(participante);
		
	}

	
	/**
	 * @param indice posi��o do participante na lista
	 * @param valor  dados do participante selecionado
	 */
	@Override
	public void moveTo(int indice, Object valor) {
		formulario.setObject((Participante) valor);
        formulario.refresh();
	}
}
