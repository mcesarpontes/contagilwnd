package gov.rfb.contagil.sped.efd.janelas;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import gov.rfb.contagil.sped.efd.EFD;

public class TabRadioButton {

	JRadioButton participantesButton;
	JRadioButton notasIcmsButton;
	JRadioButton notasIssButton;
	TabPanel tabPanel;

	public JPanel build(EFD dados) {

		participantesButton = new JRadioButton("Participantes");
		notasIcmsButton = new JRadioButton("Notas ICMS");
		notasIssButton = new JRadioButton("Notas ISS");

		participantesButton.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent action) {
			}
		});
		
		notasIcmsButton.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent action) {
			}
		});
		
		notasIssButton.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent action) {
			}
		});
		
		
		ButtonGroup bottonGroup = new ButtonGroup();
		
		bottonGroup.add(participantesButton);
		bottonGroup.add(notasIcmsButton);
		bottonGroup.add(notasIssButton);
		
		JPanel buttonPanel = new JPanel();
		
		buttonPanel.setLayout(new GridLayout(3,1));
		buttonPanel.setOpaque(true);
		
		buttonPanel.add(participantesButton);
		buttonPanel.add(notasIcmsButton);
		buttonPanel.add(notasIssButton);
		
		return buttonPanel;

	}

}
