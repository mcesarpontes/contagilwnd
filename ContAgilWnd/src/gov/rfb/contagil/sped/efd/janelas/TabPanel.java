package gov.rfb.contagil.sped.efd.janelas;

import java.util.List;

import javax.swing.JComponent;

import org.guga.contagil.model.CommonTableComponent;
import org.guga.contagil.model.FieldDescriptor;
import org.guga.contagil.model.GenericTableModel;

import gov.rfb.contagil.sped.efd.EFD;
import gov.rfb.contagil.sped.efd.Participante;
/**
 * Cria uma tabela do tipo CommonTableComponent que 
 * exibe as informações dos participantes de uma EFD 
 *  
 * @author Marcus Cesar Pontes
 *
 */
public class TabPanel  {

	

	private CommonTableComponent table_component;

	private FieldDescriptor[] campos;

	private List<?> dadosTable;

	private GenericTableModel<?> model;

	protected JComponent build(EFD dados) {

		// Array de FieldDescriptor
		campos = FieldDescriptor.getFieldDescriptors(Participante.class).toArray(new FieldDescriptor[0]);

		// Componente para a apresentação da tabela
		table_component = new CommonTableComponent();

		// Dados da apresentação
		dadosTable = dados.getParticipantes();
		table_component.setRelatorioTitle("Participantes");

		// Link dados-tabela
		model = new GenericTableModel<>(dadosTable, table_component, campos);
		table_component.setModel(model);

		// Configurações da tabela
		table_component.setStatusBar(true);
		table_component.setToolBar(true);

		// Cria Tabela
		table_component.build();
		
		return table_component.getComponent();
	}
	
}
