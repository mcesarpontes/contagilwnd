package gov.rfb.contagil.sped.efd.janelas;

import java.awt.Dimension;

import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import gov.rfb.contagil.sped.efd.EFD;
import gov.rfb.contagil.sped.efd.graficos.Coordenadas;
import gov.rfb.contagil.sped.efd.graficos.GraficoBarras;
import gov.rfb.contagil.sped.efd.graficos.GraficoPizza;
/**
 * TabbedPane contendo 3 abas:
 * 
 * 1- Grafico Pizza exibindo a distribui��o dos participantes pelas unidades da federa��o.
 * 
 * 2- Grafico Barra com as mesmas informa��es.
 * 
 * 3- Tabela com as informa��es dos participantes 
 *  
 * @author Marcus Cesar Pontes
 *
 */
public class TabbedPanel {

	public JTabbedPane build(EFD dados) {

		/* Cria TabbedPane */
		JTabbedPane tPane = new JTabbedPane(SwingConstants.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
		
		/*Obt�m as coordenadas com bas nos dados dos participantes */
		Coordenadas coordenadas = new Coordenadas (dados.getParticipantes());
		
        /* Adiciona primeira aba */
		tPane.add("Grafico Pizza", new GraficoPizza().build(coordenadas));
		
		/* Adiciona segunda aba */
		tPane.add("Gr�fico Barra",new GraficoBarras().build(coordenadas))	;
	
		/* Adiciona terceira aba */
		tPane.add("Participante",new TabPanel().build(dados));
		
		/* Define PreferredSize */
		tPane.setPreferredSize(new Dimension(800, 300));

		return tPane;

	}

	

}
