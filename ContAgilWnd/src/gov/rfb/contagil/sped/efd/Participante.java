package gov.rfb.contagil.sped.efd;

import java.io.IOException;
import java.io.ObjectInputStream;

import org.guga.contagil.analitico.MAD;
import org.guga.contagil.model.AFieldDescriptor;
import org.guga.contagil.model.ComparadorGenerico;
import org.guga.contagil.model.OrdemGrupo;

import gov.rfb.contagil.commons.tipos.AnGrao;
import gov.rfb.contagil.commons.tipos.CNPJ_CPF;
import gov.rfb.contagil.commons.tipos.DadoIdentificaContribuinte;
import gov.rfb.contagil.commons.tipos.ReflexiveType;
import gov.rfb.contagil.commons.utils.Persistencia;
import gov.rfb.contagil.commons.utils.Transformacao;
import gov.rfb.contagil.sped.efd.tabelas.dominio.TabelasDominio.CODIGOSUF;
import gov.rfb.contagil.sped.efd.tabelas.dominio.TabelasDominio.MUNICIPIO;

/**
 * Informações sobre os Participantes de uma EFD
 * 
 * Bloco 0 - Registro 0150
 * 
 * @author Marcus Cesar Pontes
 *
 */
@MAD(madTipo = "Participante", madNomeBreve = "Participante")
@Persistencia.Tabela(value = "Participante")
public class Participante implements java.io.Serializable, AnGrao, ReflexiveType, Comparable<Participante>,
		gov.rfb.contagil.sped.efd.scripting.Participante, DadoIdentificaContribuinte {

	private static final long serialVersionUID = 1L;

	@Persistencia.Coluna
	@Persistencia.Chave
	@AFieldDescriptor(nomeExterno = "Codigo", width = 80)
	@OrdemGrupo(ordem = 1)
	private String codigoParticipante;

	@Persistencia.Coluna
	@AFieldDescriptor(nomeExterno = "Nome", width = 300)
	@OrdemGrupo(ordem = 2)
	private String nome;

	@Persistencia.Coluna
	@Persistencia.IdentificaContribuinte
	@AFieldDescriptor(nomeExterno = "NI")
	@OrdemGrupo(ordem = 3)
	@Transformacao.CodigoIdentificacaoTransformado(CNPJ_CPF.class)
	private CNPJ_CPF ni;

	@Persistencia.Coluna
	@AFieldDescriptor(nomeExterno = "Endereco", width = 200)
	@OrdemGrupo(ordem = 5)
	private String endereco;

	@Persistencia.TabelaDominioReferenciada(MUNICIPIO.class)
	private String codigoMunicipio;

	@AFieldDescriptor(nomeExterno = "Municipio", width = 180)
	@OrdemGrupo(ordem = 6)
	private transient String nomeMunicipio;

	@Persistencia.TabelaDominioReferenciada(CODIGOSUF.class)
	private transient String codigoUF;

	@AFieldDescriptor(nomeExterno = "UF", width = 50)
	@OrdemGrupo(ordem = 7)
	private transient String nomeUF;

	
	@Persistencia.Reciproco()
	private transient EFD efd;


	/* Getters and Setters */
	
	public void setEFD(EFD efd) {
		this.efd = efd;
	}

	public EFD getEFD() {
		return efd;
	}

	public String getCodigoParticipante() {
		return codigoParticipante;
	}

	public void setCodigoParticipante(String codigo) {
		this.codigoParticipante = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setNi(long ni) {
		this.ni = new CNPJ_CPF(ni);
	}

	public long getNi() {
		return this.ni.getCodigo();
	}
	
	public CNPJ_CPF getNI() {
		return ni;
	}

	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}

	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}

	public void setNomeMunicipio() {
		nomeMunicipio = getNomeMunicipio();
	}

	public String getNomeMunicipio() {
		return MUNICIPIO.getDefault().getDescricao(codigoMunicipio);
	}

	public String getCodigoUF() {
		return this.codigoUF;
	}

	public void setCodigoUF() {
		this.codigoUF = codigoMunicipio.substring(0, 2);
	}

	public String getNomeUF() {
		return CODIGOSUF.getDefault().getDescricao(codigoUF);
	}

	public void setNomeUF() {
		this.nomeUF = getNomeUF();
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public int compareTo(Participante ref) {
		int comp = 0;
		if (this == ref) {
			return comp;
		}

		if (ref == null) {
			return +1;
		}

		comp = ComparadorGenerico.COMPARE(ni, ref.ni);
		if (comp != 0)
			return comp;

		return 0;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash = 37 * (hash + ComparadorGenerico.HASHCODE(nome));

		return 17 + hash;
	}

	@Override
	public boolean equals(Object o) {
		boolean equal = true;
		if (this == o) {
			return equal;
		}

		if (o == null) {
			return !equal;
		}

		if (!(o instanceof Participante)) {
			return !equal;
		}

		Participante ref = (Participante) o;
		equal = ComparadorGenerico.EQUALS(ni, ref.ni);
		if (!equal)
			return false;

		return equal;
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {

		in.defaultReadObject();

		setNomeMunicipio();
		
		setCodigoUF();
		
		setNomeUF();
	}

	@Override
	public String toString() {
		return this.nome + "     CNPJ/CPF:  " + this.ni+"    ";
	}

	@Override
	public long getCnpj() {
		return 0;
	}


	
	public boolean isCnpj() {
		return ni.isCNPJ();
	}

}
